/**
 * Contains support for marshalling JDK types to and from bytes.
 *
 * @since 1.0.0
 */
@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
@FieldsAreNonnullByDefault
package com.atlassian.marshalling.jdk;

import com.atlassian.annotations.nonnull.FieldsAreNonnullByDefault;
import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;
