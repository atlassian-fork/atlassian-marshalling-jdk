package com.atlassian.marshalling.jdk;

import com.atlassian.annotations.PublicApi;
import com.atlassian.marshalling.api.Marshaller;
import com.atlassian.marshalling.api.MarshallingException;
import com.atlassian.marshalling.api.MarshallingPair;
import com.atlassian.marshalling.api.Unmarshaller;

/**
 * Marshaller to simply pass on bytes
 *
 * @since 1.0.1
 */
@PublicApi
public class ByteMarshalling implements Marshaller<byte[]>, Unmarshaller<byte[]> {

    @Override
    public byte[] marshallToBytes(byte[] bytes) throws MarshallingException {
        return bytes;
    }

    @Override
    public byte[] unmarshallFrom(byte[] rawBytes) throws MarshallingException {
        return rawBytes;
    }

    /**
     * @return a {@link MarshallingPair}.
     */
    public static MarshallingPair<byte[]> pair() {
        final ByteMarshalling bm = new ByteMarshalling();
        return new MarshallingPair<>(bm, bm);
    }
}
